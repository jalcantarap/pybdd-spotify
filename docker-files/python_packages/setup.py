from setuptools import setup

setup(name='spotify_gherkin_demo',
      version='0.1',
      author='Jaime Alcantara',
      author_email='jaime1hk@gmail.com',
      license='MIT',
      install_requires=["dnspython",
                        "requests",
                        "loguru"],
      packages=['spotify_gherkin_demo'],
      zip_safe=False)