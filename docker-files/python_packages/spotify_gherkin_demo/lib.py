def get_token():
    with open("/usr/output/access_token.txt") as access_token_file:
        access_token = access_token_file.read()
    return access_token

def resolve_url(url='spotify.com'):
    from dns import resolver
    result = resolver.resolve(url, 'A')

    for ipval in result:
      return ipval.to_text()


def get_petition(petition, url='https://api.spotify.com', endpoint=None, access_token=None):
    headers = {
        'Content-Type': 'application/json',
        'Authorization': f'Bearer {access_token}'
    }
    url_endpoint = f"{url}{endpoint}"
    return petition(url_endpoint, headers=headers)


def follow_artist(data, access_token):
    import requests

    res = get_petition(petition=requests.put, endpoint=f'/v1/me/following?type=artist&ids={data}', access_token=access_token)
    return res

def following_artist(access_token):
    import requests

    res = get_petition(petition=requests.get, endpoint=f'/v1/me/following?type=artist', access_token=access_token)
    return res


def get_artist_by_id(data, access_token):
    import requests
    res = get_petition(petition=requests.get, endpoint=f'/v1/artists/{data}', access_token=access_token)
    return res
