# content of publish_article.feature

Feature: Spotify API
    API where we want to test the endpoints.

    Scenario Outline: Following artist by id
        Given I have a Spotify Access Token
        And I have internet connection
        And The artist with id <id> is <name>
        And I didn't follow the artist before

        When I follow the artist

        Then I should be following the artist

        Examples:
            | id                     | name            |
            | 53XhwfbYqKCa1cC15pYq2q | Imagine Dragons |
            | 1vCWHaC5f2uS3yhpwWbIA6 | Avicii          |