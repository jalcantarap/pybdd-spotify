# Automated Spotify API test
## Considerations
For the example you will follow Avicii and Imagine Dragons,
the tests will pass in case you didn't follow them before, considering you can reach Spotify API and that you get an API token.

## Requirements
* Docker
* Docker Compose

## Commands
Go to the directory in a terminal with ``cd`` and run
````docker-compose up --build````

Afterwards go to https://localhost:8888, log in with your spotify credentials and click on "Continue with tests" so that the pipeline continues, afterwards you will be redirected to read the report.
